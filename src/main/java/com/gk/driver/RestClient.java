package com.gk.driver;

import java.io.IOException;
import java.util.HashMap;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.HttpClientBuilder;

import com.gk.testlog.TestLog;
import com.gk.testlog.TestLog.Status;
import com.gk.util.RestRequestUtility;
import com.gk.util.RestResponseUtility;

public class RestClient implements IRestClient {
	
	private HttpClient httpClient;
	private String url;
	private RestResponseUtility restResponseUtility;
	private RestRequestUtility restRequestUtility;
	private TestLog testLog;
	
	public RestClient(String url) {
		httpClient = HttpClientBuilder.create().build();
		this.restResponseUtility = new RestResponseUtility();
		this.restRequestUtility = new RestRequestUtility();
		this.url = url;
	}
	
	public void setTestLog(TestLog testLog) {
		this.testLog = testLog;
		restResponseUtility.setTestLog(testLog);
		restRequestUtility.setTestLog(testLog);
	}

	public RestValidator get(String path) {
		return get(path, null);
	}


	public RestValidator get(String path, HashMap<String, String> headers) {
		HttpGet request = new HttpGet(url+path);
		testLog.log(Status.PASS, "Path:"+url+path);
		if(headers!=null) {
			restRequestUtility.setHeaders(request, headers);
		}
		
		HttpResponse response = null;
		try {
			response = httpClient.execute(request);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return new RestValidator(populateRestResponse(response), testLog);
	}

	public RestValidator post(String path, String body, String contentType) {
		return post(path, null, body, contentType);
	}

	public RestValidator post(String path, HashMap<String, String> headers, String body, String contentType) {
		System.out.println(url+path);
		HttpPost request = new HttpPost(url+path);
		if(headers!=null) {
			restRequestUtility.setHeaders(request, headers);
		}
		request.setEntity(restRequestUtility.getBody(body, contentType));
		HttpResponse response = null;
		
		try {
			response = httpClient.execute(request);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return new RestValidator(populateRestResponse(response), testLog);
	}

	public RestValidator put(String path, String body, String contentType) {
		return put(path, null, body, contentType);
	}

	public RestValidator put(String path, HashMap<String, String> headers, String body, String contentType) {
		System.out.println(url+path);
		HttpPut request = new HttpPut(url+path);
		if(headers!=null) {
			restRequestUtility.setHeaders(request, headers);
		}
		request.setEntity(restRequestUtility.getBody(body, contentType));
		HttpResponse response = null;
		
		try {
			response = httpClient.execute(request);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		return new RestValidator(populateRestResponse(response), testLog);
	}

	public RestValidator delete(String path) {
		System.out.println(path);
		return delete(path, null);
	}

	public RestValidator delete(String path, HashMap<String, String> headers) {
		System.out.println(url+path);
		HttpDelete request = new HttpDelete(url+path);
		if(headers!=null) {
			restRequestUtility.setHeaders(request, headers);
		}
		HttpResponse response = null;
		
		try {
			response = httpClient.execute(request);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return new RestValidator(populateRestResponse(response), testLog);
	}

	public RestValidator patch(String path, String body, String contentType) {
		return patch(path, null, body, contentType);
	}

	public RestValidator patch(String path, HashMap<String, String> headers, String body, String contentType) {
		HttpPatch request = new HttpPatch(url+path);
		if(headers!=null) {
			restRequestUtility.setHeaders(request, headers);
		}
		request.setEntity(restRequestUtility.getBody(body, contentType));
		HttpResponse response = null;
		
		try {
			response = httpClient.execute(request);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return new RestValidator(populateRestResponse(response), testLog);
	}
	
	private RestResponse populateRestResponse(HttpResponse response) {
		RestResponse restResponse = new RestResponse();
		restResponse.setResponseBody(restResponseUtility.getResponseBody(response).toString());
		restResponse.setResponseCode(response.getStatusLine().getStatusCode());
		restResponse.setResponseMessage(response.getStatusLine().getReasonPhrase());
		restResponse.setHeaders(restResponseUtility.getHeaders(response));
		return restResponse;
	}
	
	

}
