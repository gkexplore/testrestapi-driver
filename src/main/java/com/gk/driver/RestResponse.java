package com.gk.driver;

import java.util.HashMap;

import com.gk.testlog.TestLog;

public class RestResponse{
	
	private int responseCode;
	private String responseBody;
	private HashMap<String, String> headers;
	private String responseMessage;
	public TestLog testLog;
	
	public void setTestLog(TestLog testLog) {
		this.testLog = testLog;
	}
	public RestResponse() {
		headers = new HashMap<String, String>();
	}
	public int getResponseCode() {
		return responseCode;
	}
	
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	
	public String getResponseBody() {
		return responseBody;
	}
	
	public void setResponseBody(String responseBody) {
		this.responseBody = responseBody;
	}
	
	public HashMap<String, String> getHeaders() {
		return headers;
	}
	
	public void setHeader(String name, String value) {
		this.headers.put(name, value);
	}
	
	public String getHeader(String name) {
		return this.headers.get(name);
	}
	
	public void setHeaders(HashMap<String, String> headers) {
		this.headers = headers;
	}
	
	public String getResponseMessage() {
		return responseMessage;
	}
	
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	
	
	
}
