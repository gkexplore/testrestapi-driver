package com.gk.driver;

import java.util.HashMap;

/**
 * 
 * @author karthik
 *
 */
public interface IRestClient {
	
	//get
	public RestValidator get(String path);
	public RestValidator get(String path, HashMap<String, String> headers);
	
	//post
	public RestValidator post(String path, String body, String contentType);
	public RestValidator post(String path, HashMap<String, String> headers,String body, String contentType);
	
	//put
	public RestValidator put(String path, String body, String contentType);
	public RestValidator put(String path, HashMap<String, String> headers, String body, String contentType);
	
	//delete
	public RestValidator delete(String path);
	public RestValidator delete(String path, HashMap<String, String> headers);
	
	
	//patch
	public RestValidator patch(String path, String body, String contentType);
	public RestValidator patch(String path, HashMap<String, String> headers, String body, String contentType);

}
