package com.gk.driver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.gk.testlog.TestLog;
import com.gk.testlog.TestLog.Status;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class RestValidator {
	
	private RestResponse restResponse;
	
	public TestLog testLog;
	
	
	public RestValidator(RestResponse restResponse, TestLog testLog) {
		this.restResponse = restResponse;
		this.testLog = testLog;
	}
	
	public RestValidator expectCode(int expectedCode) {
		if(expectedCode==restResponse.getResponseCode()) {
			testLog.log(Status.PASS, "Expected Response Code Found::Expected :"+expectedCode+ "::"+"Actual:"+restResponse.getResponseCode());
		}else {
			testLog.log(Status.FAIL, "Incorrect response code: Expected :"+expectedCode+ "::"+"Actual:"+restResponse.getResponseCode());
		}
		//Assert.assertEquals(restResponse.getResponseCode(), expectedCode, "Incorrect response code");
		return this;
	}
	
	public RestValidator expectMessage(String expectedMessage) {
		if(expectedMessage.equals(restResponse.getResponseMessage())) {
			testLog.log(Status.PASS, "Expected Response Message Found:: Expected :"+expectedMessage+ "::"+"Actual:"+restResponse.getResponseMessage());
		}else {
			testLog.log(Status.FAIL, "Incorrect response message: Expected :"+expectedMessage+ "::"+"Actual:"+restResponse.getResponseMessage());
		}
		//Assert.assertEquals(restResponse.getResponseMessage(), expectedMessage, "Incorrect response message");
		return this;
	}
	
	public RestValidator expectHeader(String headerName, String headerValue) {
		//Assert.assertEquals(restResponse.getHeader(headerName), headerValue, "Incorrect header");
		if(restResponse.getHeader(headerName).equals(headerValue)) {
			testLog.log(Status.PASS, "Expected Header Found::Expected: ["+headerName+"="+headerValue+"]"+ "Actual:"+headerValue);
		}else {
			testLog.log(Status.FAIL, "Incorrect Header found: Expected: ["+headerName+"="+headerValue+"]"+ "Actual:"+headerValue);
		}
		return this;
	}
	
	public RestValidator expectHeaders(HashMap<String, String> headers) {
		for(Map.Entry<String, String> header: headers.entrySet()) {
			//TODO
			//Assert.assertEquals(restResponse.getHeaders().get(header.getKey()), headers.get(header.getKey()));
		}
		return this;
	}
	
	public RestValidator expectInBody(String content) {
		//Assert.assertTrue(restResponse.getResponseBody().contains(content), "Body does not contain String: "+content);
		if(restResponse.getResponseBody().contains(content)) {
			testLog.log(Status.PASS, "Expected Response Body Found:: Expected :"+content);
		}else {
			testLog.log(Status.FAIL, "Incorrect response message: Expected :"+content);
		}
		
		return this;
	}
	
	public RestValidator printBody() {
		System.out.println(restResponse.getResponseBody());
		return this;
	}
	
	public RestResponse getRestResponse() {
		return this.restResponse;
	}
	
	public String getStringValueFromResponse(String key) {
		JsonElement jelement = new JsonParser().parse(this.restResponse.getResponseBody());
	    JsonObject  jobject = jelement.getAsJsonObject();
	    return  jobject.get(key).getAsString();
	}
	
	public int getIntegerValueFromResponse(String key) {
		JsonElement jelement = new JsonParser().parse(this.restResponse.getResponseBody());
	    JsonObject  jobject = jelement.getAsJsonObject();
	    return  jobject.get(key).getAsInt();
	}
	
	public ArrayList<String> getArrayValueFromResponse(String key){
	
	    JsonParser jsonParser = new JsonParser();
	    JsonObject jo = (JsonObject)jsonParser.parse(this.restResponse.getResponseBody());
	    JsonArray jsonArr = jo.getAsJsonArray(key);
	    Gson googleJson = new Gson();
	    ArrayList<String> jsonObjList = googleJson.fromJson(jsonArr, ArrayList.class);
	    System.out.println("List size is : "+jsonObjList.size());
	                    System.out.println("List Elements are  : "+jsonObjList.toString());
		return jsonObjList;
	}
	
}
