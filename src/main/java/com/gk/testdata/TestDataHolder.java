package com.gk.testdata;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;

import com.opencsv.CSVReader;

public class TestDataHolder {
	private ArrayList<HashMap<String,String>> dataRows;

	public TestDataHolder(){
		dataRows = new ArrayList<HashMap<String,String>>();
	}

	public void addDataLocation(String fileLocation) throws FileNotFoundException {
		addDataLocation(new FileReader(fileLocation));
	}

	public void addDataLocation(Reader csvReader){
		final CSVReader cs = new CSVReader(csvReader);
		try{
			
			final String [] headings = cs.readNext();
			
			int rowNumber = 0;
			String [] nextLine;
		    
			while ((nextLine = cs.readNext()) != null) {
		    	if (dataRows.size() <= rowNumber){
		    		dataRows.add(rowNumber,new HashMap<String,String>());
		    	}
		    	for(int i = 0; i < nextLine.length; i++){
		    		dataRows.get(rowNumber).put(headings[i], nextLine[i]);
		    	}
		    	dataRows.get(rowNumber).put("rownumber", Integer.toString(rowNumber));
		    	rowNumber++;
		    }
			
		}catch(IOException ie){
			System.out.println("A data file couldn't be loaded");
		}finally {
			try {
				cs.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public Object[][] getAllDataRows(){
		Object[][] returnObject = new Object[this.dataRows.size()][1];
		for(int itemIndex = 0; itemIndex < this.dataRows.size(); itemIndex++){
			returnObject[itemIndex][0] = this.dataRows.get(itemIndex);
		}
		return returnObject;
	}

}