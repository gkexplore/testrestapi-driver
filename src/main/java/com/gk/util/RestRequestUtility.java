package com.gk.util;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

import com.gk.testlog.TestLog;

public class RestRequestUtility {
	
	public TestLog testLog;
	
	public void setTestLog(TestLog testLog) {
		this.testLog = testLog;
	}
	
	public void setHeaders(HttpRequestBase request, HashMap<String, String> headers) {
		for(Map.Entry<String, String> header: headers.entrySet()) {
			request.setHeader(header.getKey(), header.getValue());
		}
	}
	
	public StringEntity getBody(String jsonBody, String contentType) {
		StringEntity input = null;
		try {
			input = new StringEntity(jsonBody);
			input.setContentType(contentType);
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		
		return input;
	}
	
	
}
