package com.gk.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

import org.apache.http.Header;
import org.apache.http.HttpResponse;

import com.gk.testlog.TestLog;

public class RestResponseUtility {
	
	public TestLog testLog;
	
	public void setTestLog(TestLog testLog) {
		this.testLog = testLog;
	}
	
	public StringBuffer getResponseBody(HttpResponse response) {
		StringBuffer responseBody = new StringBuffer();
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String line = "";
			while((line=br.readLine())!=null) {
				responseBody.append(line);
			}
		} catch (UnsupportedOperationException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return responseBody;
	}
	
	public  HashMap<String, String> getHeaders(HttpResponse response){
		HashMap<String, String> headers = new HashMap<String, String>();
		Header[] responseHeaders = response.getAllHeaders();
		for(Header header: responseHeaders) {
			headers.put(header.getName(), header.getValue());
		}
		return headers;
	}

}
